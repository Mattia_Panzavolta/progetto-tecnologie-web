<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

if (login_check($mysqli) == true) {

  $id_current_user = $_SESSION['user_id'];

  if (!empty($_SESSION['id_item_to_review'])) {
    unset($_SESSION['id_item_to_review']);
  }

  if (!empty($_SESSION['id_read_item_review'])) {
    unset($_SESSION['id_read_item_review']);
  }

  if (isset($_POST['supplier'])) {
    $_SESSION['supplier_id'] = $_POST['supplier'];
  }

  if (isset($_POST['selected_category'])) {
    $category = $_POST['selected_category'];
    if($category == "Tutte") {
      $sql1 = "SELECT lista_cibo.nome lNome, lista_cibo.prezzo lPrezzo, lista_cibo.descrizione lDescrizione, lista_cibo.id_prodotto lIdProdotto, categorie.nome cNome
               FROM lista_cibo INNER JOIN categorie ON lista_cibo.id_categoria = categorie.id_categoria
               WHERE id_fornitore='" . $_SESSION['supplier_id'] . "'";
      $result1 = $mysqli->query($sql1);
    } else {
      $sql1 = "SELECT lista_cibo.nome lNome, lista_cibo.prezzo lPrezzo, lista_cibo.descrizione lDescrizione, lista_cibo.id_prodotto lIdProdotto, categorie.nome cNome
               FROM lista_cibo INNER JOIN categorie ON lista_cibo.id_categoria = categorie.id_categoria
               WHERE lista_cibo.id_fornitore='" . $_SESSION['supplier_id'] . "' AND categorie.nome='$category'";
      $result1 = $mysqli->query($sql1);
    }
  } else {
    $sql1 = "SELECT lista_cibo.nome lNome, lista_cibo.prezzo lPrezzo, lista_cibo.descrizione lDescrizione, lista_cibo.id_prodotto lIdProdotto, categorie.nome cNome
             FROM lista_cibo INNER JOIN categorie ON lista_cibo.id_categoria = categorie.id_categoria
             WHERE lista_cibo.id_fornitore='" . $_SESSION['supplier_id'] . "'";
    $result1 = $mysqli->query($sql1);
  }

  if (isset($_POST['quantita'], $_POST['id_prodotto'])) {
    $quantity = $_POST['quantita'];
    $id_product = $_POST['id_prodotto'];
    $sql2 = "SELECT id_prodotto FROM carrello WHERE id_prodotto='$id_product' AND id_utente='$id_current_user'";
    $result2 = $mysqli->query($sql2);

    if ($result2->num_rows > 0) {
      $sql3 = "SELECT quantita FROM carrello WHERE id_prodotto='$id_product' AND id_utente='$id_current_user'";
      $result3 = $mysqli->query($sql3);
      while($row3 = $result3->fetch_assoc()) {
        $new_quantity=$row3["quantita"]+$quantity;
      }
      if ($new_quantity <= 50) {
        $sql4 = "UPDATE carrello SET quantita='$new_quantity' WHERE id_prodotto='$id_product' AND id_utente='$id_current_user'";
      } else {
        $sql4 = "UPDATE carrello SET quantita='50' WHERE id_prodotto='$id_product' AND id_utente='$id_current_user'";
      }
      $mysqli->query($sql4);
    } else {
      $sql5="INSERT INTO `carrello` (`id_prodotto`, `quantita`, `id_utente`) VALUES ('$id_product', '$quantity', '$id_current_user')";
      $mysqli->query($sql5);
    }
  }

  $sql6 = "SELECT COUNT(id_notifica_utente) AS numero_notifiche FROM notifiche_utente WHERE id_utente='$id_current_user'";
  $result6 = $mysqli->query($sql6);
  $number_notifications = $result6->fetch_assoc();

  $sql7 = "SELECT nome FROM categorie";
  $result7 = $mysqli->query($sql7);

  $sql9 = "SELECT username FROM accounts WHERE id ='$id_current_user'";
  $result9 = $mysqli->query($sql9);
  $current_username = $result9->fetch_assoc();
}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>

        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <a href="list_suppliers.php" class="btn btn-primary align-self-start col-sm-1" data-toggle="tooltip" data-placement="top" title="Pagina precedente" role="button">
              <i class="fas fa-arrow-alt-circle-left fa-lg"></i>
            </a>
            <span class="col-sm-2"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <p class="col-sm-2" style="color: white;">Utente: <?php echo $current_username["username"]; ?></p>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
          <div class="col-sm-8">

            <div class="container" style="margin-top: 30px; font-size: 24px">
              <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                  <a href="catalog.php" class="nav-link active">Catalogo</a>
                </li>
                <li class="nav-item">
                  <a href="shopping_cart.php" class="nav-link">Carrello</a>
                </li>
                <li class="nav-item">
                  <a href="notifications.php" class="nav-link">Notifiche
                    <?php if($number_notifications["numero_notifiche"] > 0) { ?>
                      <span class="badge badge-light"><?php echo $number_notifications["numero_notifiche"] ?></span>
                    <?php } ?>
                  </a>
                </li>
              </ul>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <h3 class="text-center" style="padding-top: 40px">Seleziona un prodotto da aggiungere al carrello</h3>

                <form action="catalog.php" method="post" name="filter_list_form">
                  <div class='row' style="padding-top: 50px">
                    <span class="col-sm-3"></span>
                    <label class="col-sm-2" for="selected_category">Filtra per categoria:</label>
                    <select class="form-control col-sm-2" name="selected_category" id="selected_category">
                      <?php if (isset($_POST['selected_category'])) { ?>
                        <option>Tutte</option>
                        <?php if ($result7->num_rows > 0) {
                          while($row7 = $result7->fetch_assoc()) {
                            if( $row7["nome"] == $_POST['selected_category']) { ?>
                              <option selected><?php echo $row7["nome"]; ?></option>
                            <?php } else { ?>
                              <option><?php echo $row7["nome"]; ?></option>
                            <?php }
                          }
                        }
                      } else { ?>
                        <option>Tutte</option>
                        <?php if ($result7->num_rows > 0) {
                          while($row7 = $result7->fetch_assoc()) { ?>
                            <option><?php echo $row7["nome"]; ?></option>
                          <?php }
                        }
                      } ?>
                    </select>
                    <span class="col-sm-1"></span>
                    <button type="submit" class="btn btn-primary col-sm-1" data-toggle="tooltip" data-placement="right" title="Filtra">
                      <i class="fas fa-arrow-alt-circle-right fa-lg"></i>
                    </button>
                    <span class="col-sm-3"></span>
                  </div>
                </form>

                <div class="table-responsive" style="padding-top: 20px">
                  <table class="table table-striped table-hover">
                    <thead class="table-primary">
                      <tr>
                        <th>Nome</th>
                        <th>Categoria</th>
                        <th>Descrizione</th>
                        <th>Prezzo</th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if ($result1->num_rows > 0) {
                        // output data of each row
                        while($row1 = $result1->fetch_assoc()) { ?>
                          <tr>
                            <td><?php echo $row1["lNome"]; ?></td>
                            <td><?php echo $row1["cNome"]; ?></td>
                            <td style="word-break: break-all;"><?php echo $row1["lDescrizione"]; ?></td>
                            <td style="white-space:nowrap"><?php echo $row1["lPrezzo"]; ?> €</td>
                            <td>
                              <form action="reviews.php" method="post">
                                <button type="submit" name="read_reviews" value="<?php echo $row1["lIdProdotto"]; ?>"class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Leggi recensioni">
                                  <i class="fas fa-book-open fa-lg"></i>
                                </button>
                              </form>
                            </td>
                            <td>
                              <form action="write_review.php" method="post">
                                <button type="submit" name="write_review" value="<?php echo $row1["lIdProdotto"]; ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Scrivi recensione">
                                  <i class="fas fa-pencil-alt fa-lg"></i>
                                </button>
                              </form>
                            </td>
                            <td>
                              <button type="button" class="btn btn-primary quantity_button" value="<?php echo $row1["lIdProdotto"]; ?>" data-toggle="modal" data-target="#quantityModal">
                                <i class="fas fa-cart-plus fa-lg"></i>
                              </button>
                            </td>
                          </tr>
                        <?php }
                      } ?>
                    </tbody>
                  </table>
                </div>

              </div>
            </div>

          </div>
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
        </div>

        <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px;"></footer>

        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
          });
        </script>

        <script>
          $(".quantity_button").click(function() {
            var id = $(this).val();
            document.getElementById("id_prodotto").value = id;
          })
        </script>

        <div class="modal fade" id="quantityModal" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header bg-danger">
                <h5><span class="modal-title"></span> Digita la quantità che vuoi ordinare di questo prodotto</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <form class="form-horizontal" action="catalog.php" method="post" role="form">
                <div class="modal-body" style="padding:40px 50px;">
                  <div class="form-group">
                    <div class="row">
                      <span class="col-sm-3"></span>
                      <label class="control-label col-sm-6" for="quantita"> Quantità</label>
                      <span class="col-sm-3"></span>
                    </div>
                    <div class="row">
                      <span class="col-sm-3"></span>
                      <input type="number" class="form-control-number col-sm-6" name="quantita" id="quantita" value="1" min="1" max="50" required>
                      <span class="col-sm-3"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="hidden" for="id_prodotto"></label>
                    <input type="hidden" class="hidden" name="id_prodotto" value="id_prodotto" id="id_prodotto">
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name"add-to-shopping-cart" value="add-to-shopping-cart">Aggiungi al carrello</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Indietro</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

      <?php } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
