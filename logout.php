<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

$sql = "DELETE FROM carrello WHERE id_utente='" . $_SESSION['user_id'] . "'";
$mysqli->query($sql);

// Elimina tutti i valori della sessione.
$_SESSION = array();
// Recupera i parametri di sessione.
$params = session_get_cookie_params();
// Cancella i cookie attuali.
setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
// Cancella la sessione.
session_destroy();
header('Location: ./login.php');

?>
