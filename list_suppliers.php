<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

if (login_check($mysqli) == true) {

  if (!empty($_SESSION['supplier_id'])) {
    unset($_SESSION['supplier_id']);
  }

  $id_current_user = $_SESSION['user_id'];

  $sql1 = "SELECT username, id FROM accounts WHERE ruolo='fornitore'";
  $result1 = $mysqli->query($sql1);

  $sql2 = "DELETE FROM carrello WHERE id_utente='" . $_SESSION['user_id'] . "'";
  $mysqli->query($sql2);

  $sql3 = "SELECT username FROM accounts WHERE id ='$id_current_user'";
  $result3 = $mysqli->query($sql3);
  $current_username = $result3->fetch_assoc();

}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>

        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <span class="col-sm-3"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <p class="col-sm-2" style="color: white;">Utente: <?php echo $current_username["username"]; ?></p>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover; height: 100%">
          </span>
          <div class="col-sm-8">
            <?php if ($result1->num_rows > 0) { ?>
              <h3 class="text-center" style="padding-top: 40px">Seleziona un fornitore</h3>
              <div class="table-responsive" style="padding-top: 20px">
                <table class="table table-striped table-hover">
                  <thead class="table-primary">
                    <tr>
                      <th style="width:80%">Nome</th>
                      <th style="width:20%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php while($row1 = $result1->fetch_assoc()) { ?>
                      <tr>
                        <td><?php echo $row1["username"]; ?></td>
                        <td><form action="catalog.php" method="post">
                          <button type="submit" name="supplier" value="<?php echo $row1["id"]; ?>" class="btn btn-primary">
                            <i class="fas fa-arrow-alt-circle-right fa-lg"></i>
                          </button></form>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            <?php } else { ?>
              <div class="container" style="margin-top: 150px; font-size: 30px;">
                <p class="text-center">Non sono presenti fornitori al momento</p>
              </div>
            <?php } ?>
          </div>
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover; height: 100%">
          </span>
        </div>
        <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px;"></footer>

      <?php } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
