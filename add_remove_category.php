<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

if (login_check($mysqli) == true) {

  $id_current_user = $_SESSION['user_id'];

  $sql1 = "SELECT id_categoria, nome FROM categorie";
  $result1 = $mysqli->query($sql1);

  $sql2 = "SELECT username FROM accounts WHERE id ='$id_current_user'";
  $result2 = $mysqli->query($sql2);
  $current_username = $result2->fetch_assoc();

}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>

        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <span class="col-sm-3"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <p class="col-sm-2" style="color: white;">Amministratore: <?php echo $current_username["username"]; ?></p>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
          <div class="col-sm-8">

            <div class="container" style="margin-top: 30px; font-size: 24px">
              <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                  <a href="administrator.php" class="nav-link">Accounts</a>
                </li>
                <li class="nav-item">
                  <a href="add_remove_category.php" class="nav-link active">Categorie</a>
                </li>
              </ul>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <h3 class="text-center" style="padding-top: 40px">Lista delle categorie create</h3>
                <div class="table-responsive" style="padding-top: 50px;">
                  <table class="table table-striped">
                    <thead class="table-primary">
                      <tr>
                        <th>Nome</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if ($result1->num_rows > 0) {
                        // output data of each row
                        while($row1 = $result1->fetch_assoc()) { ?>
                          <tr>
                            <td><?php echo $row1["nome"]; ?></td>
                          </tr>
                        <?php }
                      } ?>
                    </tbody>
                  </table>
                </div>

                <div class="row" style="margin-top: 100px">
                  <span class="col-sm-1"></span>
                  <a href="add_category.php" class="btn btn-primary col-sm-4" role="button" style="border-radius: 25px; font-size: 20px;">
                    Aggiungi nuova categoria
                  </a>
                  <span class="col-sm-2"></span>
                  <a href="remove_category.php" class="btn btn-primary col-sm-4" role="button" style="border-radius: 25px; font-size: 20px;">
                    Rimuovi categoria
                  </a>
                  <span class="col-sm-1"></span>
                </div>

              </div>
            </div>

          </div>
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
        </div>
        <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px;"></footer>

      <?php } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
