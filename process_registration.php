<?php

include 'db_connect.php';
if(isset($_POST['username'], $_POST['email'], $_POST['pwd'], $_POST['role'])) {
  if (empty($_POST['username']) || empty($_POST['email']) || empty($_POST['pwd']) || empty($_POST['role'])) {
    $_SESSION['error_registration'] = "Errore: tutti i campi devono essere riempiti!";
  } else {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $role = $_POST['role'];
    // Recupero la password criptata dal form di inserimento.
    $password = $_POST['pwd'];
    // Crea una chiave casuale
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
    // Crea una password usando la chiave appena creata.
    $password = hash('sha512', $password.$random_salt);
    // Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
    // Assicurati di usare statement SQL 'prepared'.
    if ($insert_stmt = $mysqli->prepare("INSERT INTO accounts (username, email, password, ruolo, salt) VALUES (?, ?, ?, ?, ?)")) {
      $insert_stmt->bind_param('sssss', $username, $email, $password, $role, $random_salt);
      // Esegui la query ottenuta.
      $insert_stmt->execute();
    }
    header('Location: ./login.php');
    exit();
  }
} else {
  $_SESSION['error_registration'] = "Errore: tutti i campi devono essere riempiti!";
}

header('Location: ./registration.php');
exit();

?>
