<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <h1 class="text-center bg-danger" style="padding: 30px 0px; color:white; font-size:100px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
        <strong>Fast Delivery</strong>
      </h1>
      <div class="row">
        <span class="col-sm-3">
          <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
          background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
        </span>
        <div class="col-sm-6">
          <div class="text-center" id="error" style="color: red; margin-top: 20px; font-size: 18px">
            <?php if (!empty($_SESSION['error_login'])) {
              echo $_SESSION['error_login'];
              unset($_SESSION['error_login']);
            } ?>
          </div>
          <form action="process_login.php" method="post" name="login_form" class="needs-validation" style="padding-top: 40px;">
            <div class="form-group">
              <div class="row">
                <label for="email">Email:</label>
              </div>
              <div class="row">
                <input type="email" class="form-control" id="email" placeholder="Inserisci email" name="email" required>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label for="pwd">Password:</label>
                <span class="col-sm-3"></span>
              </div>
              <div class="row">
                <input type="password" class="form-control" id="password" placeholder="Inserisci password" name="password" required>
              </div>
            </div>
            <div class="row" style="padding-top: 40px">
              <span class="col-sm-2"></span>
              <button type="submit" class="btn btn-primary col-sm-8" style="border-radius: 25px; font-size: 22px;">Login</button>
              <span class="col-sm-2"></span>
            </div>
          </form>
          <div class="row" style="padding-top: 40px">
            <span class="col-sm-2"></span>
            <a href="registration.php" class="btn btn-info col-sm-8" role="button" style="border-radius: 25px; font-size: 22px;">Registrati</a>
            <span class="col-sm-2"></span>
          </div>
        </div>
        <span class="col-sm-3">
          <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
          background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
        </span>

      </div>
      <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px 0px;"></footer>
    </div>
  </body>
</html>
