<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

if (login_check($mysqli) == true) {

  $id_current_user = $_SESSION['user_id'];

  if(isset($_POST['write_review'])) {
    $_SESSION['id_item_to_review'] = $_POST['write_review'];
  }

  if(isset($_POST['review'])) {
    if (empty($_POST['review'])) {
      $_SESSION['error_write_review'] = "Errore: tutti i campi devono essere riempiti!";
    } else {
      $vote = $_POST['vote'];
      $text = $_POST['review'];
      $date = date("Y-m-d");
      $id_item = $_SESSION['id_item_to_review'];

      $sql1="INSERT INTO `recensioni` (`voto`, `testo`, `data_recensione`, `id_prodotto`, `id_utente`)
        VALUES ('$vote', '$text', '$date', '$id_item', '$id_current_user')";
      $mysqli->query($sql1);

      header('Location: ./catalog.php');
      exit();
    }
  }

  $sql2 = "SELECT username FROM accounts WHERE id ='$id_current_user'";
  $result2 = $mysqli->query($sql2);
  $current_username = $result2->fetch_assoc();

}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>
        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <a href="catalog.php" class="btn btn-primary align-self-start col-sm-1" data-toggle="tooltip" data-placement="top" title="Pagina precedente" role="button">
              <i class="fas fa-arrow-alt-circle-left fa-lg"></i>
            </a>
            <span class="col-sm-2"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <p class="col-sm-2" style="color: white;">Utente: <?php echo $current_username["username"]; ?></p>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-3">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
          <div class="col-sm-6">
            <h3 class="text-center" style="padding-top: 40px">Scrivi una recensione per questo prodotto</h3>
            <?php if (!empty($_SESSION['error_write_review'])) { ?>
              <div class="text-center" id="error" style="color: red; margin-top: 20px; font-size: 18px">
               <?php echo $_SESSION['error_write_review'];
               unset($_SESSION['error_write_review']);
             } ?>

            <form action="write_review.php" method="post" class="form-horizontal">
              <div class="form-group">
                <div class="row">
                  <label class="control-label" for="review">Recensione:</label>
                </div>
                <div class="row">
                  <textarea class="form-control" name="review" id="review" rows="3" maxlength="200" required></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="control-label" for="vote">Voto:</label>
                </div>
                <div class="row">
                  <select class="form-control" name="vote" id="vote">
                    <option>0</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="row" style="padding-top: 40px">
                  <span class="col-sm-2"></span>
                  <button type="submit" class="btn btn-primary btn-lg col-sm-8" style="border-radius: 25px; font-size: 20px;">
                    Invia
                  </button>
                  <span class="col-sm-2"></span>
                </div>
              </div>
            </form>
          </div>
          <span class="col-sm-3">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
        </div>
        <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px;"></footer>

        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
          });
        </script>

      <?php } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
