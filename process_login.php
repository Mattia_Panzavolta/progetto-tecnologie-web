<?php

include 'db_connect.php';
include 'functions.php';
sec_session_start(); // usiamo la nostra funzione per avviare una sessione php sicura
if(isset($_POST['email'], $_POST['password'])) {
   $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
   $password = $_POST['password']; // Recupero la password criptata.
   if(login($email, $password, $mysqli) == true) {
      // Login eseguito
      $sql1 = "SELECT ruolo FROM accounts WHERE id='" . $_SESSION['user_id'] . "'";
      $result = $mysqli->query($sql1);
      $role = $result->fetch_assoc();
      if ($role["ruolo"] == "utente") {
        header('Location: ./list_suppliers.php');
        exit();
      } else if($role["ruolo"] == "fornitore") {
        header('Location: ./add_remove_food.php');
        exit();
      } else if($role["ruolo"] == "admin") {
        header('Location: ./administrator.php');
        exit();
      }
   } else {
      // Login fallito
      $_SESSION['error_login'] = "Errore: le credenziali inserite non sono corrette!";
      header('Location: ./login.php');
      exit();
   }
} else {
  $_SESSION['error_login'] = "Errore: tutti i campi devono essere riempiti!";
  header('Location: ./login.php');
  exit();
}

?>
