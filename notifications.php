<?php

include_once 'db_connect.php';
include_once 'functions.php';

sec_session_start();

if (login_check($mysqli) == true) {

  $id_current_user = $_SESSION['user_id'];

  if (!empty($_SESSION['id_notif_supplier'])) {
    unset($_SESSION['id_notif_supplier']);
  }

  $sql1 = "SELECT ruolo FROM accounts WHERE id = '$id_current_user'";
  $result1 = $mysqli->query($sql1);
  $role = $result1->fetch_assoc();

  if(isset($_POST['delete_user_notif'])) {
    $message_to_delete = $_POST['delete_user_notif'];
    $sql4 = "DELETE FROM notifiche_utente WHERE id_notifica_utente='$message_to_delete'";
    $mysqli->query($sql4);
  }

  //user query
  if ($role["ruolo"] == "utente") {
    $sql2 = "SELECT messaggio, id_fornitore, id_notifica_utente, data FROM notifiche_utente WHERE id_utente = '$id_current_user'";
    $result2 = $mysqli->query($sql2);
    $sql12 = "SELECT COUNT(id_notifica_utente) AS numero_notifiche FROM notifiche_utente WHERE id_utente='$id_current_user'";
    $result12 = $mysqli->query($sql12);
    $number_notifications = $result12->fetch_assoc();
  } else { //supplier query
    $sql2 = "SELECT id_notifica_fornitore, id_utente, data FROM notifiche_fornitore WHERE id_fornitore = '$id_current_user'";
    $result2 = $mysqli->query($sql2);
    $sql12 = "SELECT COUNT(id_notifica_fornitore) AS numero_notifiche FROM notifiche_fornitore WHERE id_fornitore='$id_current_user'";
    $result12 = $mysqli->query($sql12);
    $number_notifications = $result12->fetch_assoc();
  }

  $sql13 = "SELECT username FROM accounts WHERE id ='$id_current_user'";
  $result13 = $mysqli->query($sql13);
  $current_username = $result13->fetch_assoc();

  $tot=0.00;

}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>

        <?php //User side
        if ($role["ruolo"] == "utente") { ?>

          <header class="bg-danger" style="padding: 50px">
            <div class="row">
              <a href="list_suppliers.php" class="btn btn-primary align-self-start col-sm-1" data-toggle="tooltip" data-placement="top" title="Pagina precedente" role="button">
                <i class="fas fa-arrow-alt-circle-left fa-lg"></i>
              </a>
              <span class="col-sm-2"></span>
              <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
                <strong>Fast Delivery</strong>
              </h1>
              <p class="col-sm-2" style="color: white;">Utente: <?php echo $current_username["username"]; ?></p>
              <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
            </div>
          </header>

          <div class="row">
            <span class="col-sm-2">
              <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
              background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
            </span>
            <div class="col-sm-8">

              <div class="container" style="margin-top: 30px; font-size: 24px">
                <ul class="nav nav-tabs nav-justified">
                  <li class="nav-item">
                    <a href="catalog.php" class="nav-link">Catalogo</a>
                  </li>
                  <li class="nav-item">
                    <a href="shopping_cart.php" class="nav-link">Carrello</a>
                  </li>
                  <li class="nav-item">
                    <a href="notifications.php" class="nav-link active">Notifiche
                      <?php if($number_notifications["numero_notifiche"] > 0) { ?>
                        <span class="badge badge-light"><?php echo $number_notifications["numero_notifiche"] ?></span>
                      <?php } ?>
                    </a>
                  </li>
                </ul>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <?php if ($result2->num_rows > 0) { ?>
                    <h3 class="text-center" style="padding-top: 40px">Lista delle notifiche ricevute</h3>
                    <div class="table-responsive" style="padding-top: 20px">
                      <table class="table table-striped table-hover">
                        <tbody>
                          <?php while($row2 = $result2->fetch_assoc()) {
                            $sql3 = "SELECT username FROM accounts WHERE id='" . $row2['id_fornitore'] . "'";
                            $result3 = $mysqli->query($sql3);
                            $row3 = mysqli_fetch_assoc($result3); ?>
                            <tr>
                              <td>Messaggio inviato da: <?php echo $row3["username"]; ?></td>
                              <td>il: <?php echo $row2["data"]; ?></td>
                              <td><?php echo $row2["messaggio"]; ?></td>
                              <td>
                                <button type="button" class="btn btn-primary btn_show_message" value="<?php echo $row2["id_notifica_utente"]; ?>" data-toggle="modal" data-target="#modalMessage">
                                  <i class="fas fa-times-circle fa-lg"></i>
                                </button>
                              </td>
                            </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  <?php } else { ?>
                    <div class="container" style="margin-top: 150px; font-size: 30px;">
                      <p class="text-center">Non sono presenti notifiche</p>
                    </div>
                  <?php } ?>

                </div>
              </div>
            </div>
            <span class="col-sm-2">
              <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
              background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
            </span>
          </div>
          <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px;"></footer>

          <script>
            $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
            });
          </script>

          <script>
            $('.btn_show_message').click(function () {
              var id = $(this).val();
              document.getElementById("id_message").value = id;
            })
          </script>

          <div class="modal fade" id="modalMessage" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header bg-danger">
                  <h5><span class="modal-title"></span>Eliminare questa notifica?</h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                  <form action="notifications.php" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                      <button type="submit" name="delete_user_notif" value="id_message" id="id_message" class="btn btn-primary">Sì</button>
                      <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        <?php     //supplier side
      } else { ?>

        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <span class="col-sm-3"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <p class="col-sm-2" style="color: white;">Fornitore: <?php echo $current_username["username"]; ?></p>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
          <div class="col-sm-8">

            <div class="container" style="margin-top: 30px; font-size: 24px">
              <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                  <a href="add_remove_food.php" class="nav-link">Catalogo</a>
                </li>
                <li class="nav-item">
                  <a href="notifications.php" class="nav-link active">Notifiche
                    <?php if($number_notifications["numero_notifiche"] > 0) { ?>
                      <span class="badge badge-light"><?php echo $number_notifications["numero_notifiche"] ?></span>
                    <?php } ?>
                  </a>
                </li>
              </ul>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <?php if ($result2->num_rows > 0) { ?>
                  <h3 class="text-center" style="padding-top: 40px">Lista degli ordini ricevuti</h3>
                  <div class="table-responsive" style="padding-top: 20px">
                    <table class="table table-striped table-hover">
                      <tbody>
                        <?php while($row2 = $result2->fetch_assoc()) {
                          $sql11 = "SELECT username FROM accounts WHERE id = '" . $row2['id_utente'] . "'";
                          $result11 = $mysqli->query($sql11);
                          $row11 = mysqli_fetch_assoc($result11);?>
                          <tr>
                            <td>Ordine inviato da: <?php echo $row11["username"]; ?></td>
                            <td>il: <?php echo $row2["data"]; ?></td>
                            <td>
                              <form action="show_notification.php" method="post">
                                <button type="submit" name="id_notification" value="<?php echo $row2["id_notifica_fornitore"]; ?>"
                                  class="btn btn-primary">Leggi
                                </button>
                              </form>
                            </td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                <?php } else { ?>
                  <div class="container" style="margin-top: 200px; font-size: 40px;">
                    <p class="text-center">Non sono presenti notifiche</p>
                  </div>
                <?php } ?>

              </div>
            </div>

          </div>
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
        </div>

        <div class="bg-danger footer" style="margin-top: 100px; padding: 30px"></div>

      <?php }

      } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
