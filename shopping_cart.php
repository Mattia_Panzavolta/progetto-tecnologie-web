<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

if (login_check($mysqli) == true) {

  $id_current_user = $_SESSION['user_id'];

  if (isset($_POST['delete'])) {
    $product_to_delect = $_POST['id_prodotto_da_eliminare'];
    $sql3 = "DELETE FROM carrello WHERE id_prodotto='$product_to_delect' AND id_utente='$id_current_user'";
    $mysqli->query($sql3);
  }

  if(isset($_POST['validate'])) {
    $status_order='In attesa';
    $destination = $_POST['destination'];
    $date = date("Y-m-d H:i");
    $sql4="INSERT INTO `notifiche_fornitore` (`id_utente`, `id_fornitore`, `stato_ordine`, `destinazione`, `data`)
      VALUES ('$id_current_user', '" . $_SESSION['supplier_id'] . "', '$status_order', '$destination', '$date')";
    $mysqli->query($sql4);
    $last_id = $mysqli->insert_id;

    $sql5 = "SELECT id_prodotto, quantita FROM carrello WHERE id_utente='$id_current_user'";
    $result5 = $mysqli->query($sql5);
    while($row5 = $result5->fetch_assoc()) {
      $sql6="INSERT INTO `lista_ordinati` (`id_notifica`, `id_prodotto`, `quantita`)
        VALUES ('$last_id', '" . $row5['id_prodotto'] . "','" . $row5['quantita'] . "')";
      $mysqli->query($sql6);
    }

    $sql7 = "DELETE FROM carrello WHERE id_utente='$id_current_user'";
    $mysqli->query($sql7);
  }

  $sql1 = "SELECT id_prodotto, quantita FROM carrello WHERE id_utente ='$id_current_user'";
  $result1 = $mysqli->query($sql1);

  $sql8 = "SELECT COUNT(id_notifica_utente) AS numero_notifiche FROM notifiche_utente WHERE id_utente='$id_current_user'";
  $result8 = $mysqli->query($sql8);
  $number_notifications = $result8->fetch_assoc();

  $sql9 = "SELECT username FROM accounts WHERE id ='$id_current_user'";
  $result9 = $mysqli->query($sql9);
  $current_username = $result9->fetch_assoc();

  $tot=0.00;

}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>

        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <a href="list_suppliers.php" class="btn btn-primary align-self-start col-sm-1" data-toggle="tooltip" data-placement="top" title="Pagina precedente" role="button">
              <i class="fas fa-arrow-alt-circle-left fa-lg"></i>
            </a>
            <span class="col-sm-2"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <p class="col-sm-2" style="color: white;">Utente: <?php echo $current_username["username"]; ?></p>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
          <div class="col-sm-8">

            <div class="container" style="margin-top: 30px; font-size: 24px">
              <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                  <a href="catalog.php" class="nav-link">Catalogo</a>
                </li>
                <li class="nav-item">
                  <a href="shopping_cart.php" class="nav-link active">Carrello</a>
                </li>
                <li class="nav-item">
                  <a href="notifications.php" class="nav-link">Notifiche
                    <?php if($number_notifications["numero_notifiche"] > 0) { ?>
                      <span class="badge badge-light"><?php echo $number_notifications["numero_notifiche"] ?></span>
                    <?php } ?>
                  </a>
                </li>
              </ul>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <?php if ($result1->num_rows > 0) { ?>
                  <h3 class="text-center" style="padding-top: 40px">Lista dei prodotti aggiunti al carrello</h3>
                  <div class="table-responsive" style="padding-top: 50px">
                    <table class="table table-striped">
                      <thead class="table-primary">
                        <tr>
                          <th>Nome</th>
                          <th>Quantità</th>
                          <th>Prezzo</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php while($row1 = $result1->fetch_assoc()) {
                          $sql2 = "SELECT nome, prezzo FROM lista_cibo WHERE id_prodotto='" . $row1['id_prodotto'] . "'";
                          $result2 = $mysqli->query($sql2);
                          $row2 = mysqli_fetch_assoc($result2);
                          $tot = $tot + $row2["prezzo"] * $row1["quantita"];?>
                          <tr>
                            <td><?php echo $row2["nome"]; ?></td>
                            <td><?php echo $row1["quantita"]; ?></td>
                            <td><?php echo $row2["prezzo"]; ?> €</td>
                            <td>
                              <button type="button" class="btn btn-primary btn_id_delete" value="<?php echo $row1["id_prodotto"]; ?>" data-toggle="modal" data-target="#modalRemove">
                                <i class="fas fa-times-circle fa-lg"></i>
                              </button>
                            </td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>

                  <div class="row" style="margin-top: 80px">
                    <span class="col-sm-5"></span>
                    <p class="text-center col-sm-2" style="font-size: 18px">Totale: <?php $tot = sprintf("%.2f", $tot); echo $tot; ?> €</p>
                    <span class="col-sm-5"></span>
                  </div>
                  <div class="row" style="margin-top: 20px">
                    <span class="col-sm-4"></span>
                    <button type="button" class="btn btn-primary btn-lg col-sm-4" data-toggle="modal" data-target="#modalValidate"
                      style="border-radius: 25px; font-size: 20px;">Invia ordine
                    </button>
                    <span class="col-sm-4"></span>
                  </div>
                <?php } else { ?>
                  <div class="container" style="margin-top: 150px; font-size: 30px;">
                    <p class="text-center" style="color: black;">Non sono presenti prodotti all'interno del carrello</p>
                  </div>
                <?php } ?>

              </div>
            </div>

          </div>
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
        </div>
        <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px;"></footer>

        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
          });
        </script>

        <script>
          $(".btn_id_delete").click(function() {
            var id = $(this).val();
            document.getElementById("id_prodotto_da_eliminare").value = id;
          })
        </script>

        <div class="modal fade" id="modalRemove" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header bg-danger">
                <h5 class="modal-title">Rimuovere questo prodotto dal carrello?</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <form action="shopping_cart.php" method="post" class="form-horizontal">
                  <div class="form-group">
                    <label class="hidden" for="id_prodotto_da_eliminare"></label>
                    <input type="hidden" class="hidden" name="id_prodotto_da_eliminare" value="id_prodotto_da_eliminare" id="id_prodotto_da_eliminare">
                  </div>
                  <div class="form-group modal-footer">
                    <button type="submit" name="delete" value="delete" class="btn btn-primary">Sì</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modalValidate" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header bg-danger">
                <h5 class="modal-title">Inserire il luogo di consegna e confermare l'ordine</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <form action="shopping_cart.php" method="post" class="form-horizontal">
                  <div class="form-group">
                    <div class="row">
                      <span class="col-sm-2"></span>
                      <label class="control-label col-sm-8" for="destination"> Destinazione</label>
                      <span class="col-sm-2"></span>
                    </div>
                    <div class="row">
                      <span class="col-sm-2"></span>
                      <input type="text" class="form-control col-sm-8" name="destination" id="destination" maxlength="70" required>
                      <span class="col-sm-2"></span>
                    </div>
                  </div>
                  <div class="form-group modal-footer">
                    <button type="submit" name="validate" value="validate" class="btn btn-primary">Sì</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

      <?php } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
