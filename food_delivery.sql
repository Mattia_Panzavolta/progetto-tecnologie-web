-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Set 10, 2019 alle 08:38
-- Versione del server: 10.1.35-MariaDB
-- Versione PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `food_delivery`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(128) NOT NULL,
  `ruolo` varchar(255) NOT NULL,
  `salt` char(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `email`, `password`, `ruolo`, `salt`) VALUES
(1, 'Mattia', 'mattiapanzavolta@gmail.com', '99d729816e3a45bab43b2dfa633765e1001612ebda8614d9caaadae8e611b96214346a8b28f6df2b5c3225b104797a08784715406a5ffd71e34ff2b2834ad07b', 'admin', '1aa184a51b5f35551bf1a0efb558b26545b36e4949c81f817fcaaa4e3f7d02064d22b83dc3737efa839578a492957c18c16f83cf7daae3d55eda3da511578407'),
(2, 'Giacomo', 'email2@gmail.com', '4b7a0cb95ae033b3a9b2a8ee74b3409d29ca6dcb6fbc62ee7443a809a35e54e3da53a094d58c94a1339ffbd35e307614ca6085c7e63ad01679b1f2abf6c6da5f', 'utente', '062f523581197f1f7dfe6c8cd97352c3a5dd07aab0225059b6d9cf32d3fee973c08e442b42123e7c34451d63fc2b662e5319c190bf44a07f0bcf05330c5d35dc'),
(3, 'Take Away', 'email3@gmail.com', '63686b7f37372ad0e8bc5aa9c130f24db2c89c0b812ec005833e362a0b43842373de6e42dbe1f94dff42d5ff7bea0ec6c0e700b9abbb37c7b7639787fb959d6c', 'fornitore', '950ef0b05d6c428e00ed574d91246ebf380645736fa782687d1848d6a6f426003c938b7cfa35563add708d00543bd2dc9301af012bcf81cd26de60e79f642b35'),
(4, 'Giuseppe Verdi', 'email4@gmail.com', '31fa0276ba85ab4fd9b017ae169db343b7480419eb111b229717bd80c4a6329c9d94724715b951cf1d8af39a9a4a53921b64aeadd8881f83171f2e182eddc46f', 'utente', '21303e5a398098886aa5fb391563b2b41b9b19dcb9002a22125ea26e054bcb61888f168fd5951763543c1f9d5f7d7e9b449ac0baa705ba2a8033dc5aa6fad9c5'),
(5, 'Food Driver', 'email5@gmail.com', 'dea50f83d95b291f4c9b8ca7d79b1c128480442cc4015bb9e755c9bd33c598f3a196516097249353f9b7d870e39393fbb730f53ddc0d86f6a6534324fc2a7864', 'fornitore', 'f434712ccf09f2193a3bfce71be14ad86180f53199a69511653500622019e2133556d417265785d051e622421b62ee0d4121c91148918d07a6fcbe05ece597e0'),
(6, 'MarioRossi', 'email6@gmail.com', 'daae5c3a8406419080f4afe447edfab818ce994b9666578d903babba73c80b40269617d7dd968ce330640cff7efceec55899af035070c47110039485cd9852c8', 'utente', '51a1a7d323afefd3069a3134fbf5c1ae93225c57a8b4a73faab5ae5ddec69375ae091c67203d3fcc7a29b1702451369f24f1f73f0d28b1c5323b0bce3614e210'),
(7, 'HomePizza', 'email7@gmail.com', 'da9c414e4e0aafa442bd3ffe83c4b9acc3a5e2e4d5caf2e9f5b17fc8fd7b31592798f2c843c27375917e5a4755beb3427ce9f7a15363dc50a4a1152f278abe0f', 'fornitore', 'f928350cdfcb610debebd7598e0f747675a63ddfff53e35b50430a607d1d7b4405b698a7894c5972ce3497754880ce9c55c2bfc684393f166dce3d5dd9ac088f');

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `id_carrello` int(11) NOT NULL,
  `id_prodotto` int(255) NOT NULL,
  `quantita` int(255) NOT NULL,
  `id_utente` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE `categorie` (
  `id_categoria` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `categorie`
--

INSERT INTO `categorie` (`id_categoria`, `nome`) VALUES
(1, 'Pasta'),
(2, 'Pizza'),
(3, 'Hamburger'),
(4, 'Sandwich'),
(5, 'Carne'),
(6, 'Frutta'),
(7, 'Verdura'),
(8, 'Dolce'),
(9, 'Bevanda'),
(10, 'Contorno'),
(11, 'Pesce');

-- --------------------------------------------------------

--
-- Struttura della tabella `lista_cibo`
--

CREATE TABLE `lista_cibo` (
  `id_prodotto` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `prezzo` float(10,2) NOT NULL,
  `descrizione` varchar(255) NOT NULL,
  `id_categoria` int(128) NOT NULL,
  `id_fornitore` int(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `lista_cibo`
--

INSERT INTO `lista_cibo` (`id_prodotto`, `nome`, `prezzo`, `descrizione`, `id_categoria`, `id_fornitore`) VALUES
(1, 'Dakota', 4.50, 'Pane, hamburger, formaggio, bacon grigliato e lattuga ', 3, 3),
(2, 'Chicken nuggets', 4.00, 'Bocconcini di pollo fritti', 5, 3),
(3, 'Patatine fritte', 2.00, 'Porzione media di patatine fritte', 10, 3),
(4, 'Fanta', 1.50, 'Lattina da 0,5L', 9, 3),
(5, 'Coca-cola', 1.50, 'Lattina da 0,5L', 9, 3),
(6, 'Cheeseburger', 3.50, 'Pane, hamburger, formaggio Cheddar, pomodori, insalata, ketchup e maionese', 3, 3),
(7, 'Insalata vegetariana', 5.00, 'Lattuga, pomodoro, verdure alla griglia e avocado', 7, 5),
(8, 'Acqua', 0.50, 'Bottiglietta da 0,5L', 9, 5),
(9, 'Sorbetto', 2.00, 'Sorbetto fresco al gusto di limone', 8, 5),
(10, 'Macedonia di frutta', 5.50, 'Pera, pesca, kiwi, banana e mela ', 6, 5),
(11, 'Spaghetti allo scoglio', 7.00, 'Spaghetti, olio, prezzemolo e vongole', 1, 5),
(12, 'Margherita', 4.50, 'Pomodoro, mozzarella', 2, 7),
(13, 'Vegetariana', 5.00, 'Pomodoro, mozzarella, peperoni, prezzemolo, zucchine, melanzane', 2, 7),
(14, 'Funghi', 5.50, 'Mozzarella, pomodoro, funghi', 2, 7),
(15, '4 stagioni', 6.00, 'Mozzarella, pomodoro, funghi, olive, carciofi, prosciutto', 2, 7),
(16, 'Capricciosa', 5.00, 'Mozzarella, pomodoro, prosciutto, olive, carciofi, funghi, salamino piccante', 2, 7),
(17, 'Crudo', 5.00, 'Mozzarella, pomodoro, prosciutto crudo', 2, 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `lista_ordinati`
--

CREATE TABLE `lista_ordinati` (
  `id_ordinato` int(11) NOT NULL,
  `id_notifica` int(255) NOT NULL,
  `id_prodotto` int(255) NOT NULL,
  `quantita` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `lista_ordinati`
--

INSERT INTO `lista_ordinati` (`id_ordinato`, `id_notifica`, `id_prodotto`, `quantita`) VALUES
(4, 2, 7, 1),
(5, 3, 4, 1),
(6, 3, 1, 1),
(7, 3, 6, 14),
(8, 4, 9, 1),
(11, 6, 1, 1),
(12, 7, 16, 1),
(13, 7, 12, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche_fornitore`
--

CREATE TABLE `notifiche_fornitore` (
  `id_notifica_fornitore` int(11) NOT NULL,
  `id_utente` int(255) NOT NULL,
  `id_fornitore` int(255) NOT NULL,
  `stato_ordine` varchar(255) NOT NULL,
  `destinazione` varchar(255) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `notifiche_fornitore`
--

INSERT INTO `notifiche_fornitore` (`id_notifica_fornitore`, `id_utente`, `id_fornitore`, `stato_ordine`, `destinazione`, `data`) VALUES
(2, 4, 5, 'In attesa', 'Via Vittoria 5', '2019-09-10'),
(3, 4, 3, 'In attesa', 'Via Vittoria 5', '2019-09-10'),
(4, 2, 5, 'In attesa', 'Via Marco Polo 12', '2019-09-10'),
(6, 2, 3, 'In attesa', 'Via Marco Polo 12', '2019-09-10'),
(7, 6, 7, 'In attesa', 'Viale Marconi 7', '2019-09-10');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche_utente`
--

CREATE TABLE `notifiche_utente` (
  `id_notifica_utente` int(11) NOT NULL,
  `id_utente` int(255) NOT NULL,
  `messaggio` varchar(255) NOT NULL,
  `id_fornitore` int(255) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `notifiche_utente`
--

INSERT INTO `notifiche_utente` (`id_notifica_utente`, `id_utente`, `messaggio`, `id_fornitore`, `data`) VALUES
(1, 2, 'Il tuo ordine e stato spedito', 3, '2019-09-10'),
(2, 2, 'Il tuo ordine e stato consegnato', 3, '2019-09-10'),
(3, 2, 'Il tuo ordine Ã¨ stato spedito', 7, '2019-09-10'),
(4, 2, 'Il tuo ordine Ã¨ stato consegnato', 7, '2019-09-10');

-- --------------------------------------------------------

--
-- Struttura della tabella `recensioni`
--

CREATE TABLE `recensioni` (
  `id_recensione` int(11) NOT NULL,
  `voto` int(10) NOT NULL,
  `testo` varchar(255) NOT NULL,
  `data_recensione` date NOT NULL,
  `id_prodotto` int(128) DEFAULT NULL,
  `id_utente` int(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `recensioni`
--

INSERT INTO `recensioni` (`id_recensione`, `voto`, `testo`, `data_recensione`, `id_prodotto`, `id_utente`) VALUES
(1, 5, 'Ottima pizza!', '2019-09-10', 12, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `tentativi_login`
--

CREATE TABLE `tentativi_login` (
  `id_utente` int(255) NOT NULL,
  `data_login` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`id_carrello`),
  ADD KEY `id_prodotto` (`id_prodotto`),
  ADD KEY `id_utente` (`id_utente`);

--
-- Indici per le tabelle `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indici per le tabelle `lista_cibo`
--
ALTER TABLE `lista_cibo`
  ADD PRIMARY KEY (`id_prodotto`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `id_fornitore` (`id_fornitore`);

--
-- Indici per le tabelle `lista_ordinati`
--
ALTER TABLE `lista_ordinati`
  ADD PRIMARY KEY (`id_ordinato`),
  ADD KEY `id_notifica` (`id_notifica`),
  ADD KEY `id_prodotto` (`id_prodotto`);

--
-- Indici per le tabelle `notifiche_fornitore`
--
ALTER TABLE `notifiche_fornitore`
  ADD PRIMARY KEY (`id_notifica_fornitore`),
  ADD KEY `id_utente` (`id_utente`),
  ADD KEY `id_fornitore` (`id_fornitore`);

--
-- Indici per le tabelle `notifiche_utente`
--
ALTER TABLE `notifiche_utente`
  ADD PRIMARY KEY (`id_notifica_utente`),
  ADD KEY `id_utente` (`id_utente`),
  ADD KEY `id_fornitore` (`id_fornitore`);

--
-- Indici per le tabelle `recensioni`
--
ALTER TABLE `recensioni`
  ADD PRIMARY KEY (`id_recensione`),
  ADD KEY `id_prodotto` (`id_prodotto`),
  ADD KEY `id_utente` (`id_utente`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `carrello`
--
ALTER TABLE `carrello`
  MODIFY `id_carrello` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT per la tabella `lista_cibo`
--
ALTER TABLE `lista_cibo`
  MODIFY `id_prodotto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT per la tabella `lista_ordinati`
--
ALTER TABLE `lista_ordinati`
  MODIFY `id_ordinato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT per la tabella `notifiche_fornitore`
--
ALTER TABLE `notifiche_fornitore`
  MODIFY `id_notifica_fornitore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `notifiche_utente`
--
ALTER TABLE `notifiche_utente`
  MODIFY `id_notifica_utente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `recensioni`
--
ALTER TABLE `recensioni`
  MODIFY `id_recensione` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `carrello`
--
ALTER TABLE `carrello`
  ADD CONSTRAINT `carrello_ibfk_1` FOREIGN KEY (`id_prodotto`) REFERENCES `lista_cibo` (`id_prodotto`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `carrello_ibfk_2` FOREIGN KEY (`id_utente`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `lista_cibo`
--
ALTER TABLE `lista_cibo`
  ADD CONSTRAINT `lista_cibo_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorie` (`id_categoria`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lista_cibo_ibfk_2` FOREIGN KEY (`id_fornitore`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `lista_ordinati`
--
ALTER TABLE `lista_ordinati`
  ADD CONSTRAINT `lista_ordinati_ibfk_1` FOREIGN KEY (`id_notifica`) REFERENCES `notifiche_fornitore` (`id_notifica_fornitore`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lista_ordinati_ibfk_2` FOREIGN KEY (`id_prodotto`) REFERENCES `lista_cibo` (`id_prodotto`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `notifiche_fornitore`
--
ALTER TABLE `notifiche_fornitore`
  ADD CONSTRAINT `notifiche_fornitore_ibfk_1` FOREIGN KEY (`id_utente`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `notifiche_fornitore_ibfk_2` FOREIGN KEY (`id_fornitore`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `notifiche_utente`
--
ALTER TABLE `notifiche_utente`
  ADD CONSTRAINT `notifiche_utente_ibfk_1` FOREIGN KEY (`id_utente`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `notifiche_utente_ibfk_2` FOREIGN KEY (`id_fornitore`) REFERENCES `lista_cibo` (`id_prodotto`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `recensioni`
--
ALTER TABLE `recensioni`
  ADD CONSTRAINT `recensioni_ibfk_1` FOREIGN KEY (`id_prodotto`) REFERENCES `lista_cibo` (`id_prodotto`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `recensioni_ibfk_2` FOREIGN KEY (`id_utente`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
