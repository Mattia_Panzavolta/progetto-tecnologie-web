<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

if (login_check($mysqli) == true) {

  $id_current_user = $_SESSION['user_id'];

  if(isset($_POST['id_notification'])) {
    $_SESSION['id_notif_supplier'] = $_POST['id_notification'];
  }

  if(isset($_POST['send_order'])) {
    $message = 'Il tuo ordine è stato spedito';
    $id_user = $_POST['id_user'];
    $id_notif = $_POST['send_order'];
    $date = date("Y-m-d H:i");
    $sql5 = "UPDATE notifiche_fornitore SET stato_ordine='Spedito' WHERE id_notifica_fornitore='$id_notif'";
    $mysqli->query($sql5);
    $sql6="INSERT INTO `notifiche_utente` (`id_utente`, `messaggio`, `id_fornitore`, `data`)
      VALUES ('$id_user', '$message', '$id_current_user', '$date')";
    $mysqli->query($sql6);
  }

  if(isset($_POST['order_delivered'])) {
    $message='Il tuo ordine è stato consegnato';
    $id_user= $_POST['id_user'];
    $id_notif = $_POST['order_delivered'];
    $date = date("Y-m-d H:i");
    $sql7 = "UPDATE notifiche_fornitore SET stato_ordine='Consegnato' WHERE id_notifica_fornitore='$id_notif'";
    $mysqli->query($sql7);
    $sql8="INSERT INTO `notifiche_utente` (`id_utente`, `messaggio`, `id_fornitore`, `data`)
      VALUES ('$id_user', '$message', '$id_current_user', '$date')";
    $mysqli->query($sql8);
  }

  if(isset($_POST['delete_order'])) {
    $id_notif = $_POST['delete_order'];
    $sql9 = "DELETE FROM notifiche_fornitore WHERE id_notifica_fornitore='$id_notif'";
    $mysqli->query($sql9);
    $sql10 = "DELETE FROM lista_ordinati WHERE id_notifica='$id_notif'";
    $mysqli->query($sql10);
    header('Location: ./notifications.php');
    exit();
  }

  $sql1 = "SELECT username FROM accounts WHERE id ='$id_current_user'";
  $result1 = $mysqli->query($sql1);
  $current_username = $result1->fetch_assoc();

  $sql2 = "SELECT id_prodotto, quantita FROM lista_ordinati WHERE id_notifica='" . $_SESSION['id_notif_supplier'] . "'";
  $result2 = $mysqli->query($sql2);

  $sql4 = "SELECT stato_ordine, id_utente, destinazione FROM notifiche_fornitore WHERE id_notifica_fornitore='" . $_SESSION['id_notif_supplier'] . "'";
  $result4 = $mysqli->query($sql4);
  $row4 = mysqli_fetch_assoc($result4);

  $tot=0.00;

}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>
        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <a href="notifications.php" class="btn btn-primary align-self-start col-sm-1" data-toggle="tooltip" data-placement="top" title="Pagina precedente" role="button">
              <i class="fas fa-arrow-alt-circle-left fa-lg"></i>
            </a>
            <span class="col-sm-2"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <p class="col-sm-2" style="color: white;">Fornitore: <?php echo $current_username["username"]; ?></p>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
          <div class="col-sm-8">
            <h3 class="text-center" style="padding-top: 40px">Lista dei prodotti all'interno dell'ordine</h3>
            <div class="table-responsive" style="padding-top: 60px">
              <table class="table table-striped">
                <thead class="table-primary">
                  <tr>
                    <th>Nome</th>
                    <th>Quantità</th>
                    <th>Prezzo</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while($row2 = $result2->fetch_assoc()) {
                    $sql3 = "SELECT nome, prezzo FROM lista_cibo WHERE id_prodotto='" . $row2['id_prodotto'] . "'";
                    $result3 = $mysqli->query($sql3);
                    $row3 = mysqli_fetch_assoc($result3);
                    $tot = $tot + $row3["prezzo"] * $row2["quantita"]; ?>
                    <tr>
                      <td><?php echo $row3["nome"] ?></td>
                      <td><?php echo $row2["quantita"] ?></td>
                      <td><?php echo $row3["prezzo"] ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>

            <div class="text-center" style="margin-top: 20px">
              <p style="font-size: 18px">Totale: <?php $tot = sprintf("%.2f", $tot); echo $tot; ?> €</p>
            </div>

            <div class="text-center" style="margin-top: 20px">
              <p style="font-size: 20px">Destinazione consegna: <?php echo $row4["destinazione"]; ?></p>
            </div>

            <div style="margin-top: 40px">
              <form action="show_notification.php" method="post" class="form-horizontal">
                <div class="form-group text-center">
                  <?php if($row4["stato_ordine"] == "In attesa") {?>
                    <button type="submit" name="send_order" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" style="border-radius: 15px; font-size: 20px;">Spedisci ordine</button>
                    <button type="submit" name="order_delivered" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" disabled style="border-radius: 15px; font-size: 20px;">Ordine consegnato</button>
                    <button type="submit" name="delete_order" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" disabled style="border-radius: 15px; font-size: 20px;">Elimina ordine</button>
                  <?php } else if($row4["stato_ordine"] == "Spedito") {?>
                    <button type="submit" name="send_order" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" disabled style="border-radius: 15px; font-size: 20px;">Spedisci ordine</button>
                    <button type="submit" name="order_delivered" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" style="border-radius: 15px; font-size: 20px;">Ordine consegnato</button>
                    <button type="submit" name="delete_order" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" disabled style="border-radius: 15px; font-size: 20px;">Elimina ordine</button>
                  <?php } else { ?>
                    <button type="submit" name="send_order" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" disabled style="border-radius: 15px; font-size: 20px;">Spedisci ordine</button>
                    <button type="submit" name="order_delivered" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" disabled style="border-radius: 15px; font-size: 20px;">Ordine consegnato</button>
                    <button type="submit" name="delete_order" value="<?php echo $_SESSION['id_notif_supplier'] ?>" class="btn btn-primary" style="border-radius: 15px; font-size: 20px;">Elimina ordine</button>
                  <?php } ?>
                  <label class="hidden" for="id_user"></label>
                  <input type="hidden" class="hidden" name="id_user" value="<?php echo $row4["id_utente"]; ?>">
                </div>
              </form>
            </div>
          </div>
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
        </div>

        <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px;"></footer>

        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
          });
        </script>

      <?php } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
