<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

if (login_check($mysqli) == true) {

  $id_current_user = $_SESSION['user_id'];

  if (!empty($_SESSION['id_read_item_review'])) {
    unset($_SESSION['id_read_item_review']);
  }

  $sql1 = "SELECT lista_cibo.nome lNome, lista_cibo.prezzo lPrezzo, lista_cibo.descrizione lDescrizione, lista_cibo.id_prodotto lIdProdotto, categorie.nome cNome
           FROM lista_cibo INNER JOIN categorie ON lista_cibo.id_categoria = categorie.id_categoria
           WHERE id_fornitore='$id_current_user'";
  $result1 = $mysqli->query($sql1);

  $sql2 = "SELECT COUNT(id_notifica_fornitore) AS numero_notifiche FROM notifiche_fornitore WHERE id_fornitore='$id_current_user'";
  $result2 = $mysqli->query($sql2);
  $number_notifications = $result2->fetch_assoc();

  $sql3 = "SELECT username FROM accounts WHERE id ='$id_current_user'";
  $result3 = $mysqli->query($sql3);
  $current_username = $result3->fetch_assoc();
}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>

        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <span class="col-sm-3"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <p class="col-sm-2" style="color: white;">Fornitore: <?php echo $current_username["username"]; ?></p>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
          <div class="col-sm-8">

            <div class="container" style="margin-top: 30px; font-size: 24px">
              <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                  <a href="add_remove_food.php" class="nav-link active">Catalogo</a>
                </li>
                <li class="nav-item">
                  <a href="notifications.php" class="nav-link">Notifiche
                    <?php if($number_notifications["numero_notifiche"] > 0) { ?>
                      <span class="badge badge-light"><?php echo $number_notifications["numero_notifiche"] ?></span>
                    <?php } ?>
                  </a>
                </li>
              </ul>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <h3 class="text-center" style="padding-top: 40px">Lista dei prodotti nel catalogo</h3>
                <div class="table-responsive" style="padding-top: 50px;">
                  <table class="table table-striped">
                    <thead class="table-primary">
                      <tr>
                        <th>Nome</th>
                        <th>Categoria</th>
                        <th>Descrizione</th>
                        <th>Prezzo</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if ($result1->num_rows > 0) {
                        // output data of each row
                        while($row1 = $result1->fetch_assoc()) { ?>
                          <tr>
                            <td><?php echo $row1["lNome"]; ?></td>
                            <td><?php echo $row1["cNome"]; ?></td>
                            <td style="word-break: break-all;"><?php echo $row1["lDescrizione"]; ?></td>
                            <td style="white-space:nowrap"><?php echo $row1["lPrezzo"]; ?> €</td>
                            <td>
                              <form action="reviews.php" method="post">
                                <button type="submit" name="read_reviews" value="<?php echo $row1["lIdProdotto"]; ?>"class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Leggi recensioni">
                                  <i class="fas fa-book-open fa-lg"></i>
                                </button>
                              </form>
                            </td>
                          </tr>
                        <?php }
                      } ?>
                    </tbody>
                  </table>
                </div>
                <div class="row" style="margin-top: 100px">
                  <span class="col-sm-1"></span>
                  <a href="add_food.php" class="btn btn-primary col-sm-4" role="button" style="border-radius: 25px; font-size: 20px;">
                    Aggiungi nuovo prodotto
                  </a>
                  <span class="col-sm-2"></span>
                  <a href="remove_food.php" class="btn btn-primary col-sm-4" role="button" style="border-radius: 25px; font-size: 20px;">
                    Rimuovi prodotto
                  </a>
                  <span class="col-sm-1"></span>
                </div>
              </div>
            </div>

          </div>
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
        </div>

        <footer class="bg-danger footer" style="margin-top: 100px; padding: 40px;"></footer>

        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
          });
        </script>

      <?php } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
