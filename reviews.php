<?php

include_once 'db_connect.php';
include_once 'functions.php';
sec_session_start();

if (login_check($mysqli) == true) {

  $id_current_user = $_SESSION['user_id'];

  if(isset($_POST['read_reviews'])){
    $_SESSION['id_read_item_review'] = $_POST['read_reviews'];
  }

  $id_item = $_SESSION['id_read_item_review'];

  $sql1 = "SELECT accounts.username aUsername, recensioni.voto rVoto, recensioni.testo rTesto, recensioni.data_recensione rDataRecensione
           FROM recensioni INNER JOIN accounts ON accounts.id = recensioni.id_utente
           WHERE recensioni.id_prodotto = '$id_item'";
  $result1 = $mysqli->query($sql1);

  $sql2 = "SELECT username, ruolo FROM accounts WHERE id ='$id_current_user'";
  $result2 = $mysqli->query($sql2);
  $current_account = $result2->fetch_assoc();

}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Progetto TW</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <div class="container-fluid">
      <?php if (login_check($mysqli) == true) { ?>
        <header class="bg-danger" style="padding: 50px">
          <div class="row">
            <?php if ($current_account["ruolo"] == "utente") { ?>
              <a href="catalog.php" class="btn btn-primary align-self-start col-sm-1" data-toggle="tooltip" data-placement="top" title="Pagina precedente" role="button">
                <i class="fas fa-arrow-alt-circle-left fa-lg"></i>
              </a>
            <?php } else { ?>
              <a href="add_remove_food.php" class="btn btn-primary align-self-start col-sm-1" data-toggle="tooltip" data-placement="top" title="Pagina precedente" role="button">
                <i class="fas fa-arrow-alt-circle-left fa-lg"></i>
              </a>
            <?php } ?>
            <span class="col-sm-2"></span>
            <h1 class="text-center col-sm-6" style="padding: 5px 0px; color:white; font-size: 50px; font-family: 'Bangers', cursive; text-shadow: 5px 5px 0 rgba(0, 0, 0, 0.7)">
              <strong>Fast Delivery</strong>
            </h1>
            <?php if ($current_account["ruolo"] == "utente") { ?>
              <p class="col-sm-2" style="color: white;">Utente: <?php echo $current_account["username"]; ?></p>
            <?php } else { ?>
              <p class="col-sm-2" style="color: white;">Fornitore: <?php echo $current_account["username"]; ?></p>
            <?php } ?>
            <a href="logout.php" class="btn btn-primary align-self-start col-sm-1" role="button">Logout</a>
          </div>
        </header>

        <div class="row">
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
          <div class="col-sm-8">
            <?php if ($result1->num_rows > 0) { ?>
              <h3 class="text-center" style="padding-top: 40px">Leggi le recensioni sul prodotto selezionato</h3>
              <div class="table-responsive" style="margin-top: 50px">
                <table class="table table-striped">
                  <thead class="table-primary">
                    <tr>
                      <th>Username</th>
                      <th>Recensione</th>
                      <th>Voto</th>
                      <th>Data</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php while($row1 = $result1->fetch_assoc()) { ?>
                      <tr>
                        <td><?php echo $row1["aUsername"]; ?></td>
                        <td><?php echo $row1["rTesto"]; ?></td>
                        <td><?php echo $row1["rVoto"]; ?></td>
                        <td><?php echo $row1["rDataRecensione"]; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            <?php } else { ?>
              <div class="container" style="margin-top: 150px; font-size: 40px;">
                <p class="text-center">Non sono presenti recensioni per questo articolo</p>
              </div>
            <?php } ?>
          </div>
          <span class="col-sm-2">
            <img class="img-fluid" src="immagini_cibo/food_background_4.jpg" alt="food_background" style="
            background-position: center; background-repeat: no-repeat; background-size: cover;  height: 100%;">
          </span>
        </div>
        <div class="bg-danger footer" style="margin-top: 100px; padding: 40px"></div>

        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
          });
        </script>

      <?php } else { ?>
        <p>
          <span class="error">You are not authorized to access this page.</span>
        </p>
      <?php } ?>

    </div>
  </body>
</html>
